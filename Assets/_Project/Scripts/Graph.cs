﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph
{
    public class GraphEdge
    {
        private GraphVertex m_Source;
        private GraphVertex m_Target;
        private float m_Weight;

        public GraphEdge(GraphVertex source, GraphVertex target, float weight)
        {
            Source = source;
            Target = target;
            Weight = weight;
        }

        public GraphVertex Source { get => m_Source; set => m_Source = value; }
        public GraphVertex Target { get => m_Target; set => m_Target = value; }
        public float Weight { get => m_Weight; set => m_Weight = value; }
    }               

    public class GraphVertex
    {
        private List<GraphEdge> m_Edges; //Points to the edges that start from this vertex. This facilitates iteration (e.g. A*)
        private Vector2 m_Position;
        private int m_InstanceIndex; //Each vertex has a unique index used both to identify and to classify

        public GraphVertex(Vector2 position, int instanceIndex)
        {
            Position = position;
            InstanceIndex = instanceIndex;
            m_Edges = new List<GraphEdge>();
        }

        public void AddEgde(GraphEdge edge)
        {
            if (edge.Source == this)
            {
                m_Edges.Add(edge);
            }
        }

        public Vector2 Position { get => m_Position; private set => m_Position = value; }
        public int InstanceIndex { get => m_InstanceIndex; private set => m_InstanceIndex = value; }
        public List<GraphEdge> Edges { get => m_Edges; private set => m_Edges = value; }
    }

    private List<GraphVertex> m_Vertices;
    private List<GraphEdge> m_Edges;

    public List<GraphVertex> Vertices
    {
        get
        {
            return new List<GraphVertex>(m_Vertices);
        }
    }
    public List<GraphEdge> Edges
    {
        get
        {
            return new List<GraphEdge>(m_Edges);
        }
    }

    public Graph()
    {
        m_Vertices = new List<GraphVertex>();
        m_Edges = new List<GraphEdge>();
    }

    public int AddVertex(Vector2 position)
    {
        int index = m_Vertices.Count;
        m_Vertices.Add(new GraphVertex(position, index));
        return index;
    }

    public void AddEdge(GraphVertex source, GraphVertex target, float weight)
    {
        GraphEdge edge = new GraphEdge(source, target, weight);
        m_Edges.Add(edge);
        source.AddEgde(edge);
    }
    public void AddEdge(int sourceIndex, int targetIndex, float weight)
    {
        if (m_Vertices.Count > sourceIndex && m_Vertices.Count > targetIndex)
        {
            AddEdge(m_Vertices[sourceIndex], m_Vertices[targetIndex], weight);
        }
    }
}
