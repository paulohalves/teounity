﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphRenderer : MonoBehaviour
{
    [SerializeField] private ColorPalette _colorPalette;
    [SerializeField] private Material _lineMaterial;
    [SerializeField] private Sprite _vertexSprite;
    private Graph m_Graph;

    private GameObject lines;
    private GameObject points;

    public void CreateGraph(Graph graph)
    {
        m_Graph = graph;

        var vertices = graph.Vertices;
        var egdes = graph.Edges;

        lines = new GameObject("Lines");
        lines.transform.parent = transform;

        foreach (Graph.GraphEdge edge in egdes)
        {
            GameObject line = new GameObject("Line: " + edge.Source.InstanceIndex + " -> " + edge.Target.InstanceIndex);
            line.transform.parent = lines.transform;

            LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
            lineRenderer.sharedMaterial = _lineMaterial;
            lineRenderer.startColor = _colorPalette.SecondaryColor;
            lineRenderer.endColor = _colorPalette.SecondaryColor;
            lineRenderer.startWidth = lineRenderer.endWidth = 0.5f;
            lineRenderer.SetPosition(0, edge.Source.Position);
            lineRenderer.SetPosition(1, edge.Target.Position);
        }

        points = new GameObject("Points");
        points.transform.parent = transform;

        foreach (Graph.GraphVertex vertex in vertices)
        {
            GameObject point = new GameObject("Point: " + vertex.InstanceIndex);
            point.transform.parent = points.transform;
            point.transform.position = vertex.Position;

            GraphVertexHandler graphVertex = point.AddComponent<GraphVertexHandler>();
            graphVertex.GraphVertex = vertex;

            SpriteRenderer spriteRenderer = point.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = _vertexSprite;
            spriteRenderer.color = _colorPalette.SecondaryColor;
        }
    }

    public void ClearGraph()
    {
        if (lines)
        {
            Destroy(lines);
        }

        if (points)
        {
            Destroy(points);
        }
    }
}
