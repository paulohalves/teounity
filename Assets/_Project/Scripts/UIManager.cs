﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Text _pointsRemaining;

    [SerializeField] private GameObject[] _upperPanels;
    [SerializeField] private GameObject[] _lowerPanels;

    private int currentStage = 0;

    public int PointsRemaining
    {
        set
        {
            _pointsRemaining.text = "Points Remaining: " + value;
        }
    }

    public void SetStage(int stage)
    {
        if (stage >= 0 && stage < _upperPanels.Length)
        {
            _upperPanels[currentStage].SetActive(false);
            _lowerPanels[currentStage].SetActive(false);
            currentStage = stage;
            _upperPanels[currentStage].SetActive(true);
            _lowerPanels[currentStage].SetActive(true);
        }
    }

    public void NextStage()
    {
        if (currentStage < _upperPanels.Length - 1)
        {
            _upperPanels[currentStage].SetActive(false);
            _lowerPanels[currentStage].SetActive(false);
            currentStage++;
            _upperPanels[currentStage].SetActive(true);
            _lowerPanels[currentStage].SetActive(true);
        }
    }
}
