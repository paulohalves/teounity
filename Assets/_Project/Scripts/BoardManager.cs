﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    [SerializeField] private Sprite m_CircleSprite;
    [SerializeField] private Material m_LineMaterial;

    private static BoardManager instance;

    private static Dictionary<int, GameObject> m_Layers = new Dictionary<int, GameObject>();

    private void Awake()
    {
        instance = this;
    }

    public static SpriteRenderer CreateCircle(Vector2 position, float size, Color color, int layer)
    {
        GameObject parent = GetLayer(layer);

        GameObject circle = new GameObject("circle");
        circle.transform.parent = parent.transform;
        circle.transform.position = position;

        SpriteRenderer spriteRenderer = circle.AddComponent<SpriteRenderer>();
        spriteRenderer.sprite = instance.m_CircleSprite;
        spriteRenderer.color = color;
        spriteRenderer.sortingOrder = layer;
        spriteRenderer.transform.localScale = Vector3.one * size;

        return spriteRenderer;
    }

    public static LineRenderer CreateLine(Vector2 start, Vector2 end, float width, Color color, int layer = 0)
    {
        GameObject parent = GetLayer(layer);

        GameObject line = new GameObject("line");
        line.transform.parent = parent.transform;

        LineRenderer lineRenderer = line.AddComponent<LineRenderer>();

        lineRenderer.startColor = lineRenderer.endColor = color;
        lineRenderer.material = instance.m_LineMaterial;
        lineRenderer.sortingOrder = layer;
        lineRenderer.startWidth = lineRenderer.endWidth = width;
        lineRenderer.SetPosition(0, start);
        lineRenderer.SetPosition(1, end);

        return lineRenderer;
    }

    public static GameObject GetLayer(int layer)
    {
        if (!m_Layers.ContainsKey(layer))
        {
            GameObject layerObj = new GameObject("Layer " + layer);
            layerObj.transform.parent = instance.transform;
            m_Layers.Add(layer, layerObj);
            return layerObj;
        }
        else
        {
            return m_Layers[layer];
        }
    }

    public static void SetLayerVisibility(int layer, bool visibility)
    {
        if (m_Layers.ContainsKey(layer))
        {
            m_Layers[layer].SetActive(visibility);
        }
    }

    public static void ClearBoard()
    {
        var layers = new List<GameObject>(m_Layers.Values);

        foreach (var layer in layers)
        {
            Destroy(layer);
        }

        m_Layers.Clear();
    }
}
