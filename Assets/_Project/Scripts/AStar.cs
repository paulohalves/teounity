﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AStar
{
    public class AStarInfo
    {
        public AStarInfo parent;
        public Graph.GraphVertex vertex;

        public float g;
        public float h;

        public float f
        {
            get
            {
                return g + h;
            }
        }
    }

    private static Dictionary<int, Graph.GraphVertex> _vertices = new Dictionary<int, Graph.GraphVertex>();
    private static List<AStarInfo> _openList = new List<AStarInfo>();
    private static List<int> _calculatedList = new List<int>();

    public static List<Vector2> GetPath(Graph.GraphVertex source, Graph.GraphVertex target)
    {
        List<Vector2> path = new List<Vector2>();
        _openList.Clear();
        _calculatedList.Clear();

        path.Add(target.Position); //Add the target to the path list (it will always be the last one)

        if (source.InstanceIndex == target.InstanceIndex)
        {
            //Target is the source
            return path;
        }

        AStarInfo start = new AStarInfo()
        {
            parent = null,
            vertex = source,
            g = 0,
            h = Heuristic(source, target)
        };
        _openList.Add(start); //Add to the list of nodes waiting to have their neighbors discovered
        _calculatedList.Add(start.vertex.InstanceIndex); //Add to the list of caculated weights

        while (_openList.Count > 0) //While there are still nodes having neighbors to be discovered 
        {
            _openList.Sort((a, b) => a.f.CompareTo(b.f)); //Get the node with the smallest weight
            AStarInfo node = _openList[0];

            foreach (Graph.GraphEdge edge in node.vertex.Edges) //For each neightbour
            {
                if (edge.Target.InstanceIndex == target.InstanceIndex)
                {
                    //Found target
                    path.InsertRange(0, ReturnToOrigin(node));
                    return path;
                }

                if (!_calculatedList.Contains(edge.Target.InstanceIndex)) //Weights has not been calculated yet
                {
                    AStarInfo newNode = new AStarInfo()
                    {
                        parent = node,
                        vertex = edge.Target,
                        g = node.g + 1,
                        h = Heuristic(edge.Target, target)
                    };
                    _openList.Add(newNode); //Add to the list of nodes waiting to have their neighbors discovered
                    _calculatedList.Add(node.vertex.InstanceIndex); //Add to the list of caculated weights
                }
            }
            _openList.Remove(node); //All neighbour have been discovered
        }

        return null; //Did not find a path to the target
    }

    private static List<Vector2> ReturnToOrigin(AStarInfo aStarInfo)
    {
        List<Vector2> path = new List<Vector2>();

        for (AStarInfo current = aStarInfo; current != null;)
        {
            path.Insert(0, current.vertex.Position);
            current = current.parent;
        }

        return path;
    }

    private static float Heuristic(Graph.GraphVertex vertex, Graph.GraphVertex target)
    {
        return Vector2.Distance(vertex.Position, target.Position);
    }
}
