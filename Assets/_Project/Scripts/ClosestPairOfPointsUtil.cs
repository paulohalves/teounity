﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ClosestPairOfPointsUtil
{
    public static List<Vector2> ClosestPairOfPoints(this List<Vector2> points)
    {
        if (points.Count == 1)
        {
            return null;
        }
        else if (points.Count == 2)
        {
            return points;
        }
        else
        {
            points.Sort((a, b) => a.x.CompareTo(b.x));

            List<Vector2>[] p = points.Split();
            float mid = (p[0].Last().x + p[1][0].x) / 2.0f;

            List<Vector2> d1 = ClosestPairOfPoints(p[0]);
            List<Vector2> d2 = ClosestPairOfPoints(p[1]);
            float d = Mathf.Min(d1.SqrtLenght(), d2.SqrtLenght());

            List<Vector2> d3 = new List<Vector2>();
            for(int i = p[0].Count - 1; i >= 0; i++)
            {
                if (Mathf.Abs(p[0][i].x - mid) < d)
                {
                    d3.Add(p[0][i]);
                }
                else
                {
                    break;
                }
            }
            for (int i = 0; i < p[1].Count; i++)
            {
                if (Mathf.Abs(p[1][i].x - mid) < d)
                {
                    d3.Add(p[1][i]);
                }
                else
                {
                    break;
                }
            }

            return MinSqrtMagnitude(d1, d2, d3);
        }
    }

    public static List<Vector2> MinSqrtMagnitude(params List<Vector2>[] lines)
    {
        float d = Mathf.Infinity;
        int h = 0;

        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i] != null && lines[i].Count > 1)
            {
                float sM = (lines[i][0] - lines[i][1]).sqrMagnitude;
                if (sM < d)
                {
                    d = sM;
                    h = 1;
                }
            }
        }

        return lines[h];
    }

    public static float SqrtLenght(this List<Vector2> points)
    {
        float d = 0;
        if (points.Count > 1)
        {
            for (int i = 1; i < points.Count; i++)
            {
                d += (points[i] - points[i - 1]).sqrMagnitude;
            }
        }
        return d;
    }

    public static List<T>[] Split<T>(this List<T> points)
    {
        List<T>[] halfs = new List<T>[2];

        int half = Mathf.CeilToInt(points.Count / 2);
        halfs[0].AddRange(points.GetRange(0, half));
        halfs[1].AddRange(points.GetRange(half, points.Count - half));

        return halfs;
    }

    
    public static T Last<T>(this List<T> points)
    {
        return points[points.Count - 1];
    }
}
