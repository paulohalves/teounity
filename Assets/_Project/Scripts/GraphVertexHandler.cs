﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphVertexHandler : MonoBehaviour
{
    private Graph.GraphVertex m_GraphVertex;
    public Graph.GraphVertex GraphVertex { get => m_GraphVertex; set => m_GraphVertex = value; }

    private void Awake()
    {
        CircleCollider2D collider = gameObject.AddComponent<CircleCollider2D>();
        collider.radius = 1.0f;
    }
}
