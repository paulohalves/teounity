﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using csDelaunay;

public class RoutesManager : MonoBehaviour
{
    public enum Stage
    {
        PlacingPoints,
        GrahamScan,
        Voronoi,
        Graph,
        ChoosingSourcePoint,
        ChoosingTargetPoint,
        AStart
    }

    [SerializeField] private UIManager _uIManager;
    [SerializeField] private GraphRenderer _graphRenderer;
    [SerializeField] private MeshRenderer meshRenderer;

    [SerializeField] private Vector2 boardSize = new Vector2(50, 50);
    [SerializeField] private int pointsLimit = 20;

    [SerializeField] private ColorPalette _colorPalette;

    private Stage stage;
    private List<UnityEngine.Vector2> points;

    private Graph graph;
    private GrahamScan grahamScan;
    private Voronoi voronoi;

    private int currentPoints = 0;

    private SpriteRenderer grahamScanPointBeingProcessed;
    private SpriteRenderer grahamScanLastPoint;
    private SpriteRenderer grahamScanNextPoint;

    private Dictionary<Vector2, SpriteRenderer> renderedPoints = new Dictionary<Vector2, SpriteRenderer>();
    private Dictionary<Vector2, SpriteRenderer> convexHullPoints = new Dictionary<Vector2, SpriteRenderer>();

    private SpriteRenderer originPoint;
    private SpriteRenderer destinationPoint;
    private Graph.GraphVertex origin;
    private Graph.GraphVertex destination;

    private void Awake()
    {
        points = new List<UnityEngine.Vector2>();
        _uIManager.PointsRemaining = pointsLimit - currentPoints;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hitInfo;
            
            if (hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.forward, Mathf.Infinity))
            {
                if (stage == Stage.PlacingPoints)
                {
                    if (hitInfo.collider.gameObject.tag == "Point")
                    {
                        RemovePoint(hitInfo.collider.gameObject.transform.position);
                    }
                    else if (hitInfo.collider.gameObject.tag == "Board" && currentPoints < pointsLimit)
                    {
                        Vector2 point = new Vector2(hitInfo.point.x, hitInfo.point.y);
                        PlacePoint(point);
                    }
                }
                else if (stage == Stage.ChoosingSourcePoint)
                {
                    GraphVertexHandler graphVertex = hitInfo.collider.gameObject.GetComponent<GraphVertexHandler>();
                    if (graphVertex)
                    {
                        SetOriginPoint(graphVertex.GraphVertex);
                    }
                }
                else if (stage == Stage.ChoosingTargetPoint)
                {
                    GraphVertexHandler graphVertex = hitInfo.collider.gameObject.GetComponent<GraphVertexHandler>();
                    if (graphVertex)
                    {
                        if (origin != destination)
                        {
                            SetDestinationPoint(graphVertex.GraphVertex);
                        }
                    }
                }
            }
        }

        //else if (Input.GetKeyDown(KeyCode.R))
        //{
        //    grahamScan = new GrahamScan(points);
        //}
        //else if (Input.GetKeyDown(KeyCode.N))
        //{
        //    points.Clear();
        //    for (int i = 0; i < pointsCount; i++)
        //    {
        //        points.Add(new UnityEngine.Vector2(Random.Range(0, 50), Random.Range(0, 50)));
        //    }
        //
        //    grahamScan = new GrahamScan(points);
        //    grahamScan.OnFinish += HandleOnFinishGrahamScan;
        //    meshRenderer.GetComponent<MeshFilter>().mesh = grahamScan.Mesh;
        //}
    }

    public void FillRemainingPoints()
    {
        if (currentPoints >= pointsLimit)
        {
            ClearPoints();
        }
        for (int i = currentPoints; i < pointsLimit; i++)
        {
            Vector2 point = new UnityEngine.Vector2(Random.Range(0, boardSize.x), Random.Range(0, boardSize.y));
            PlacePoint(point);
        }
    }

    private void PlacePoint(Vector2 point)
    {
        points.Add(point);

        var rPoint = BoardManager.CreateCircle(point, 1.0f, Color.black, 0);
        rPoint.gameObject.AddComponent<CircleCollider2D>();
        rPoint.tag = "Point";

        renderedPoints.Add(point, rPoint);
        currentPoints++;
        _uIManager.PointsRemaining = pointsLimit - currentPoints;
    }

    private void RemovePoint(Vector2 point)
    {
        if (renderedPoints.ContainsKey(point))
        {
            Destroy(renderedPoints[point].gameObject);
            points.Remove(point);
            renderedPoints.Remove(point);
            currentPoints--;
            _uIManager.PointsRemaining = pointsLimit - currentPoints;
        }
    }

    public void ClearPoints()
    {
        var ps = new List<Vector2>(renderedPoints.Keys);

        foreach (var point in ps)
        {
            RemovePoint(point);
        }
    }

    public void DoGrahamScan()
    {
        if (points.Count >= 3)
        {
            stage = Stage.GrahamScan;
            _uIManager.NextStage();

            grahamScan = new GrahamScan(points, false);
            grahamScan.OnNextStep += HandleOnGrahamScanNextStep;
            grahamScan.OnAddConvexHullPoint += HandleOnAddConvexHullPoint;
            grahamScan.OnRemoveConvexHullPoint += HandleOnRemoveConvexHullPoint;
            grahamScan.OnFinish += HandleOnFinishGrahamScan;
            meshRenderer.GetComponent<MeshFilter>().mesh = grahamScan.Mesh;
        }
    }

    public void GrahamNextStep()
    {
        if (!grahamScan.Done)
        {
            grahamScan.Step();
        }
    }

    public void GrahamSkip()
    {
        if (!grahamScan.Done)
        {
            grahamScan.Finish();
        }
    }

    private void HandleOnAddConvexHullPoint(Vector2 point)
    {
        var p = BoardManager.CreateCircle(grahamScan.PointBeingProcessed, 1.0f, _colorPalette.PrimaryColor, 1);
        p.gameObject.name = "convexHull";
        convexHullPoints.Add(point, p);
    }

    private void HandleOnRemoveConvexHullPoint(Vector2 point)
    {
        if (convexHullPoints.ContainsKey(point))
        {
            Destroy(convexHullPoints[point].gameObject);
            convexHullPoints.Remove(point);
        }
    }

    private void HandleOnGrahamScanNextStep()
    {
        if (grahamScanPointBeingProcessed == null)
        {
            grahamScanPointBeingProcessed = BoardManager.CreateCircle(grahamScan.PointBeingProcessed, 1.0f, _colorPalette.FocusColor, 2);
        }
        else
        {
            grahamScanPointBeingProcessed.transform.position = grahamScan.PointBeingProcessed;
        }

        if (grahamScanLastPoint == null)
        {
            grahamScanLastPoint = BoardManager.CreateCircle(grahamScan.LastPoint, 1.0f, _colorPalette.StandByColor, 2);
        }
        else
        {
            grahamScanLastPoint.transform.position = grahamScan.LastPoint;
        }

        if (grahamScanNextPoint == null)
        {
            grahamScanNextPoint = BoardManager.CreateCircle(grahamScan.NextPoint, 1.0f, _colorPalette.StandByColor, 2);
        }
        else
        {
            grahamScanNextPoint.transform.position = grahamScan.NextPoint;
        }
    }

    private void HandleOnFinishGrahamScan()
    {
        BoardManager.SetLayerVisibility(2, false);

        for(int i = 0; i < grahamScan.ConvexHull.Count; i++)
        {
            BoardManager.CreateLine(grahamScan.ConvexHull[i], grahamScan.ConvexHull[(i+1) % grahamScan.ConvexHull.Count], 1f, _colorPalette.PrimaryColor, 1);
        }

        stage = Stage.Voronoi;
        _uIManager.NextStage();
    }

    public void DoVoronoi()
    {
        HandleChangeGrahamScanVisibility(false);

        voronoi = new Voronoi(points, new Rectf(0, 0, 50, 50));
        graph = new Graph();

        //Get all Voronoi sites and sort them according to the index
        List<Site> vertices = new List<Site>();
        vertices.AddRange(voronoi.SitesIndexedByLocation.Values);
        vertices.Sort((a, b) => a.SiteIndex.CompareTo(b.SiteIndex));

        //Adds all sites to the graph as vertices
        foreach (var vertex in vertices)
        {
            graph.AddVertex(vertex.Coord);
        }

        foreach (Edge edge in voronoi.Edges)
        {
            if (edge.ClippedEnds != null)
            {
                //Draw lines
                BoardManager.CreateLine(edge.ClippedEnds[LR.LEFT], edge.ClippedEnds[LR.RIGHT], 0.5f, Color.black, 0);
            }

            //If it have valid sites (at each end)
            if (edge.LeftSite == null || edge.RightSite == null)
            {
                continue;
            }

            int leftIndex = edge.LeftSite.SiteIndex;
            int rightIndex = edge.RightSite.SiteIndex;

            //Adds egdes to the graph for each vertex (for both ways)
            graph.AddEdge(leftIndex, rightIndex, Vector2.Distance(edge.LeftSite.Coord, edge.RightSite.Coord));
            graph.AddEdge(rightIndex, leftIndex, Vector2.Distance(edge.RightSite.Coord, edge.LeftSite.Coord));
        }

        stage = Stage.Graph;
        _uIManager.NextStage();
    }

    public void DoGraph()
    {
        BoardManager.SetLayerVisibility(0, false);
        _graphRenderer.CreateGraph(graph);

        stage = Stage.ChoosingSourcePoint;
        _uIManager.NextStage();
    }

    public void SetOriginPoint(Graph.GraphVertex vertex)
    {
        if (originPoint == null)
        {
            originPoint = BoardManager.CreateCircle(vertex.Position, 1.0f, _colorPalette.FocusColor, 5);
        }
        else
        {
            originPoint.transform.position = vertex.Position;
        }

        origin = vertex;
    }

    public void HandleChooseOrigin()
    {
        if (origin != null)
        {
            stage = Stage.ChoosingTargetPoint;
            _uIManager.NextStage();
        }
    }

    public void SetDestinationPoint(Graph.GraphVertex vertex)
    {
        if (destinationPoint == null)
        {
            destinationPoint = BoardManager.CreateCircle(vertex.Position, 1.0f, _colorPalette.FocusColor, 5);
        }
        else
        {
            destinationPoint.transform.position = vertex.Position;
        }

        destination = vertex;
    }

    public void HandleChooseDestination()
    {
        if (destination != null)
        {
            stage = Stage.AStart;
            _uIManager.NextStage();

            HandleChangeVoronoiVisibility(true);
            HandleChangeGrahamScanVisibility(true);

            List<Vector2> path = AStar.GetPath(origin, destination);

            for (int i = 0; i < path.Count; i++)
            {
                if (i < path.Count - 1)
                {
                    BoardManager.CreateLine(path[i], path[(i + 1) % path.Count], 1f, _colorPalette.FocusColor, 5);
                }
            }
        }
    }

    public void HandleChangeGrahamScanVisibility(bool value)
    {
        BoardManager.SetLayerVisibility(1, value);
        meshRenderer.gameObject.SetActive(value);
    }
    public void HandleChangeVoronoiVisibility(bool value)
    {
        BoardManager.SetLayerVisibility(0, value);
    }
    public void HandleChangeGraphVisibility(bool value)
    {
        _graphRenderer.gameObject.SetActive(value);
    }
    public void HandleChangeAStarVisibility(bool value)
    {
        BoardManager.SetLayerVisibility(5, value);
    }

    private void ClearGrahamScan()
    {
        var ps = new List<Vector2>(convexHullPoints.Keys);

        foreach (var point in ps)
        {
            HandleOnRemoveConvexHullPoint(point);
        }

        convexHullPoints.Clear();
    }

    public void Restart()
    {
        HandleChangeGrahamScanVisibility(true);
        HandleChangeGraphVisibility(true);

        BoardManager.ClearBoard();
        points.Clear();
        renderedPoints.Clear();
        convexHullPoints.Clear();

        _graphRenderer.ClearGraph();

        currentPoints = 0;

        meshRenderer.GetComponent<MeshFilter>().mesh = null;

        originPoint = null;
        destinationPoint = null;
        origin = null;
        destination = null;

        grahamScanPointBeingProcessed = null;
        grahamScanLastPoint = null;
        grahamScanNextPoint = null;

        stage = Stage.PlacingPoints;
        _uIManager.SetStage(0);
    }
}
