﻿using UnityEngine;

[CreateAssetMenu(fileName = "ColorPalette", menuName = "Data/ColorPalette")]
public class ColorPalette : ScriptableObject
{
    [SerializeField] private Color m_PrimaryColor;
    [SerializeField] private Color m_SecondaryColor;
    [SerializeField] private Color m_TertiaryColor;
    [SerializeField] private Color m_FocusColor;
    [SerializeField] private Color m_StandByColor;

    public Color PrimaryColor { get => m_PrimaryColor; private set => m_PrimaryColor = value; }
    public Color SecondaryColor { get => m_SecondaryColor; private set => m_SecondaryColor = value; }
    public Color TertiaryColor { get => m_TertiaryColor; private set => m_TertiaryColor = value; }
    public Color FocusColor { get => m_FocusColor; private set => m_FocusColor = value; }
    public Color StandByColor { get => m_StandByColor; private set => m_StandByColor = value; }
}
