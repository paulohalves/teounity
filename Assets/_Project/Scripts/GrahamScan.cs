﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GrahamScan
{
    public delegate void ConvexHullHandler();
    public delegate void ConvexHullPointHandler(Vector2 point);

    public event ConvexHullHandler OnFinish;
    public event ConvexHullHandler OnNextStep;
    public event ConvexHullPointHandler OnAddConvexHullPoint;
    public event ConvexHullPointHandler OnRemoveConvexHullPoint;

    private List<UnityEngine.Vector2> m_Points;
    private List<UnityEngine.Vector2> m_ConvexHull;
    private UnityEngine.Vector2 m_Origin;
    private Mesh m_Mesh;

    private Vector2 m_PointBeingProcessed;
    private Vector2 m_NextPoint;
    private Vector2 m_LastPoint;

    private int currentStep;
    private bool started = false;
    private bool done = false;
    private bool inSteps = true;

    public Mesh Mesh { get => m_Mesh; private set => m_Mesh = value; }
    public List<UnityEngine.Vector2> ConvexHull { get => m_ConvexHull; private set => m_ConvexHull = value; }
    public List<Vector2> Points { get => m_Points; private set => m_Points = value; }

    public Vector2 NextPoint { get => m_NextPoint; private set => m_NextPoint = value; }
    public Vector2 LastPoint { get => m_LastPoint; private set => m_LastPoint = value; }
    public Vector2 PointBeingProcessed { get => m_PointBeingProcessed; private set => m_PointBeingProcessed = value; }

    public bool Done { get => done; private set => done = value; }

    public GrahamScan(List<UnityEngine.Vector2> points, bool startAutomatically = true)
    {
        Mesh = new Mesh();
        m_ConvexHull = new List<Vector2>(); //This is used basically to graphical feedback reasons
        Points = new List<UnityEngine.Vector2>(points);

        if (startAutomatically)
        {
            Start();
        }
    }

    public void Start()
    {
        started = true;
        int ind = SmallerY(in m_Points); //Indentifies the point that has the smaller Y

        m_Origin = Points[ind]; //Defines the indentified point as the origin

        Points.Sort(CompareAngle); //Sorts points by comparing the angle with the point of origin
        currentStep = 1;

        PointBeingProcessed = m_Origin;
        m_ConvexHull.Add(m_Origin);
        OnAddConvexHullPoint?.Invoke(m_Origin);
    }

    public void Step() //The actual algorithm
    {
        if (!started)
        {
            Start();
        }

        if (!Done)
        {
            int i = currentStep; //Each step process a single point

            if (i == (Points.Count - 1)) //The last point. No need to test the last one. Finish process
            {
                PointBeingProcessed = Points[i];
                NextPoint = Points[0];
                LastPoint = Points[i - 1];

                OnNextStep?.Invoke();

                m_ConvexHull.Add(Points[i]);
                OnAddConvexHullPoint?.Invoke(Points[i]);

                CreateMesh();
                Done = true;
                OnFinish?.Invoke();
                return;
            }

            try
            {
                //For graphical feedback
                PointBeingProcessed = Points[i];
                if (m_ConvexHull.Count == i + 1)
                {
                    m_ConvexHull.RemoveAt(i); //Adds and removes as points are tested 
                    OnRemoveConvexHullPoint?.Invoke(Points[i]);
                }
                //

                NextPoint = Points[i + 1];
                LastPoint = Points[i - 1];

                OnNextStep?.Invoke();

                //Tests whether the point being processed forms a convex angle with its neighbors
                if (!ConvexAngle(LastPoint, PointBeingProcessed, NextPoint)) 
                {
                    //If not, the point is discarded
                    Points.RemoveAt(i);

                    if (i > 1) //No need to the origin again
                    {
                        i--; //Then, we want to test the previous point 
                    }
                }
                else
                {
                    //If angle is convex 
                    m_ConvexHull.Add(Points[i]); //Adds and removes as points are tested
                    OnAddConvexHullPoint?.Invoke(Points[i]);

                    i++; //We want to test the next point 
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError(e);
                done = true;
                return;
            }

            currentStep = i;

            if (!inSteps)
            {
                Step();
            }
        }
    }

    public void Finish()
    {
        inSteps = false;
        if (!done)
        {
            Step();
        }
    }

    private void CreateMesh()
    {
        List<int> tringules = new List<int>();
        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        foreach (UnityEngine.Vector2 point in Points)
        {
            vertices.Add(point);
            normals.Add(Vector3.forward);
        }

        for (int i = 1; i < Points.Count - 1; i++)
        {
            tringules.AddRange(new int[] { 0, i, i+1 });
        }

        Mesh.SetVertices(vertices);
        Mesh.SetNormals(normals);
        Mesh.SetTriangles(tringules, 0);
    }

    private int SmallerY(in List<UnityEngine.Vector2> points)
    {
        int h = 0;

        for (int i = 1; i < points.Count; i++)
        {
            if (points[i].y == points[h].y)
            {
                if (points[i].x <= points[h].x)
                {
                    h = i;
                }
            }
            else if (points[i].y < points[h].y)
            {
                h = i;
            }
        }

        return h;
    }

    private int CompareAngle(UnityEngine.Vector2 a, UnityEngine.Vector2 b)
    {
        float aAngle = UnityEngine.Vector2.Angle(UnityEngine.Vector2.right, a - m_Origin);
        float bAngle = UnityEngine.Vector2.Angle(UnityEngine.Vector2.right, b - m_Origin);

        int angC = aAngle.CompareTo(bAngle);

        if (angC == 0) // if angle is equals, use the one close to the origin, this also ensures the origin is the index 0
        {
            return (a - m_Origin).sqrMagnitude.CompareTo((b - m_Origin).sqrMagnitude);
        }
        else
        {
            return angC;
        }
    }

    private bool ConvexAngle(UnityEngine.Vector2 a, UnityEngine.Vector2 b, UnityEngine.Vector2 c)
    {
        // ccw > 0: counter-clockwise; ccw < 0: clockwise; ccw = 0: collinear
        float ccw = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
        return ccw > 0;
    }
}
