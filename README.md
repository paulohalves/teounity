# Técnicas de Otimização

Implementação por: Paulo Henrique Alves de Deus

## Graham Scan

Você pode encontrar a implementação do Graham Scan no script [GrahamScan.cs](https://gitlab.com/paulohalves/teounity/-/blob/master/Assets/_Project/Scripts/GrahamScan.cs).

## Grafos

Você pode encontrar a implementação do grafo no script [Graph.cs](https://gitlab.com/paulohalves/teounity/-/blob/master/Assets/_Project/Scripts/Graph.cs).

## A*

Você pode encontrar a implementação do A* no script [AStar.cs](https://gitlab.com/paulohalves/teounity/-/blob/master/Assets/_Project/Scripts/AStar.cs).

## Voronoi e demais comportamentos

Você pode encontrar os demais comportamentos – como a utilização do Voronoi, geração de núvem de pontos e etc. – no script [RoutesManager.cs](https://gitlab.com/paulohalves/teounity/-/blob/master/Assets/_Project/Scripts/RoutesManager.cs).

A biblioteca de diagramas de Voronoi utilizada foi a [csDelaunay](https://github.com/PouletFrit/csDelaunay/tree/master/Delaunay) por PouletFrit.